libextutils-modulemaker-perl (0.63-3) unstable; urgency=medium

  * Team upload.
  * Move libmodule-build-perl to B-D-I.
    Only needed for tests.
  * Add lintian override for libmodule-build-perl.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Wrap long lines in changelog entries: 0.63-2.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Jan 2023 23:21:09 +0100

libextutils-modulemaker-perl (0.63-2) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Apply multi-arch hints. + libextutils-modulemaker-perl: Add :any qualifier
    for perl dependency.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 09 Dec 2022 00:26:25 +0000

libextutils-modulemaker-perl (0.63-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Imported version 0.58
  * Update perl dependency minimal version to 5.27.5 (File::Path 2.15 is
    required here)
  * Bump debhelper compatibility level to 10

  [ gregor herrmann ]
  * Update (build) dependencies.
  * Update years of upstream copyright.
  * Import upstream version 0.62

  [ Xavier Guimard ]
  * Import upstream version 0.63
  * Declare compliance with policy 4.2.1
  * Email change: Xavier Guimard -> yadd@debian.org

 -- Xavier Guimard <yadd@debian.org>  Tue, 13 Nov 2018 21:09:19 +0100

libextutils-modulemaker-perl (0.56-1) unstable; urgency=medium

  * Team upload

  * New upstream version 0.56
  * declare conformance with Policy 4.1.1 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Tue, 21 Nov 2017 21:30:44 +0000

libextutils-modulemaker-perl (0.55-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Lucas Kanashiro ]
  * Import upstream version 0.55
  * Update debian/upstream/metadata
  * Update years of upstream copyright
  * Update years and my email in Debian packaging copyright
  * Declare compliance with Debian policy 3.9.8

 -- Lucas Kanashiro <kanashiro@debian.org>  Thu, 30 Jun 2016 10:57:17 -0300

libextutils-modulemaker-perl (0.54-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.54
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl
  * Remove t/testlib/IO/Capture* entry from d/copyright
  * Add libio-capture-perl as build dependency

  [ gregor herrmann ]
  * Update years of upstream copyright.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 21 Aug 2015 20:58:42 -0300

libextutils-modulemaker-perl (0.52-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Jul 2014 12:35:49 +0200

libextutils-modulemaker-perl (0.51-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Set HOME for test suite in debian/rules. (Closes: #713228)

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Jun 2013 17:03:36 +0200

libextutils-modulemaker-perl (0.51-1) unstable; urgency=low

  * Initial Release (Closes: #695560)

 -- Xavier Guimard <x.guimard@free.fr>  Fri, 14 Dec 2012 21:50:21 +0100
